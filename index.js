const getInputValue = (node) => {
  if (node.value === "") {
    return null;
  } else {
    return node.value;
  }
};

module.exports = getInputValue;
